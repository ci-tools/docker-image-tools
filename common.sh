# Common functions and variables

# This is the type we need to request and then receive the manifest.json file
export CONTENT_TYPE='application/vnd.docker.distribution.manifest.v2+json'

# Regular expresion to separate the registry from the image path
export REGEXP='\([^/]*\)/\([a-z0-9\._\-]*/[a-z0-9\:\._\-]*\)'

# Given the DOCKER_IMAGE returns the image path only
function extractDockerImagePath() {
    echo $1 | sed -e "s#$REGEXP#\2#"
}

# Given the DOCKER_IMAGE returns the docker server
function extractDockerServer() {
    # If the expression does not match (-n), print nothing.
    DOCKER_SERVER=$(echo $1 | sed -n "s#$REGEXP#\1#p")
    # If no specified, DOCKER_SERVER is docker hub
    DOCKER_SERVER=${DOCKER_SERVER:-hub.docker.com}

    echo $DOCKER_SERVER
}

#
# It receives a headers file with a 'Www-Authenticate' header. Extracts from this herader the Authentication URL.
#   Uses the URL and $DOCKER_LOGIN_USERNAME and $DOCKER_LOGIN_PASSWORD to get the $TOKEN
#
function getToken() {
    HEADERS_FILE=$1

    : "${DOCKER_LOGIN_USERNAME:?DOCKER_LOGIN_USERNAME not provided}"
    : "${DOCKER_LOGIN_PASSWORD:?DOCKER_LOGIN_PASSWORD not provided}"

    URL=$(grep '^Www-Authenticate' $HEADERS_FILE | sed -e 's#.*Bearer realm="\([^"]*\)",service="\([^"]*\)",scope="\([^"]*\)".*$#\1?service=\2\&scope=\3,push#')

    curl "$URL" -u "${DOCKER_LOGIN_USERNAME}:${DOCKER_LOGIN_PASSWORD}" | jq .token -r
}
