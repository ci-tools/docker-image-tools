#! /bin/sh

# This script deletes an image from the gitlab registry, from a tag name

# Variables:
# - TAG: [Mandatory] Tag pointing to the image to be deleted
# - DOCKER_LOGIN_PASSWORD: [Mandatory] Password to log into the docker registry. Defaults to the CI Token of the job
#       Due to https://gitlab.com/gitlab-org/gitlab-ce/issues/51769, the user must provide either a password
#       or a GitLab Personal Access Token in order to delete an image from the registry.
#       When using a PAT, DOCKER_LOGIN_USERNAME is not needed.
# - DOCKER_LOGIN_USERNAME: Username to log into the docker registry. Defaults to the CI Runner account
# - DOCKER_IMAGE: The docker image. Defaults to $CI_REGISTRY/$CI_PROJECT_PATH
#      environment variables, which in Gitlab-CI is the current registry/repository, i.e.: gitlab-registry.cern.ch/group/project

# Exit codes:
# 0: All good
# 1: Authentication missing
# 2: Tag to deleted missing

set -e

DOCKER_LOGIN_USERNAME=${DOCKER_LOGIN_USERNAME:-gitlab-ci-token}
DOCKER_LOGIN_PASSWORD=${DOCKER_LOGIN_PASSWORD:-${CI_JOB_TOKEN}}
DOCKER_IMAGE=${DOCKER_IMAGE_PATH:-${CI_REGISTRY}/${CI_PROJECT_PATH}}
#
# Source common variables and functions
. /usr/local/bin/common.sh


# workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380
if [ -e /.runonce ]; then
  echo "[gitlabci-docker-tools] Build already done (workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380)"
  exit 0
fi
touch /.runonce

# At the moment CI_JOB_TOKEN cannot delete images from the registry
# See: https://gitlab.com/gitlab-org/gitlab-ce/issues/51769
# Remove this check if/when it is possible
if [ -z "${DOCKER_LOGIN_PASSWORD}" ]
then
    echo "Please set DOCKER_LOGIN_PASSWORD to make the script work. At the moment, CI_JOB_TOKEN is not able to delete images from the registry (cf. https://gitlab.com/gitlab-org/gitlab-ce/issues/51769). See README.md for more information"
    exit 1
fi

if [ -z "${TAG}" ]
then
    echo "Please set TAG to make the script work. See README.md for more information"
    exit 2
fi

#
# In order to get the TOKEN, we need to make a NON authenticated request. A Www-Authenticate header
#     will be returned with the login details
#
CODE=$(curl -s "https://$DOCKER_SERVER/v2/$DOCKER_IMAGE_PATH/manifests/${OLDTAG}" -D headers.txt \
-o manifest.json -H "Accept: $CONTENT_TYPE" -w "%{http_code}")

#
# If the request returns 401 - Unauthorized, we use the WWW-Authenticate header stored in a file to
#     obtain a token from the Docker registry. This token can then be used to perform requests that
#      require authentication.
#
if [ "$CODE" == "401" ];
then
    echo "Obtain token to authenticate with the registry"
    TOKEN=$(getToken headers.txt)
fi

# As images can only be deleted by DIGEST, we have to first recover it from the current tag
echo "Obtaining digest for the '${TAG}' tag'"
DIGEST=$(curl --fail --silent -i "https://${DOCKER_SERVER}/v2/${DOCKER_IMAGE_PATH}/manifests/$TAG" -H "Authorization: Bearer ${TOKEN}" -H "Accept: $CONTENT_TYPE"  -o /dev/null -D - | grep Docker-Content-Digest | awk -F ' ' '{printf "%s",$2}')
# URL encode the digest in order to use it in the next query (requires python)
ENCODED_DIGEST=$(python -c "import urllib; print urllib.quote('''$DIGEST''')")

# Delete tag from remote registry using the previously urlencode digest
echo "Deleting '${DOCKER_IMAGE_PATH}:${TAG}' from registry..."
curl --fail --silent -X DELETE -G "https://${DOCKER_SERVER}/v2/${DOCKER_IMAGE_PATH}/manifests/${ENCODED_DIGEST}" -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -H "Authorization: Bearer ${TOKEN}"

echo "The tag ${DOCKER_IMAGE_PATH}:${TAG} has been successfully deleted"
