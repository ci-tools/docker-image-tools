#!/bin/sh

# This script allows to add a new tag to a pushed image to the registry

# Variables
#
# - OLDTAG: [Mandatory] Tag referencing an image to copy
# - NEWTAG: [Mandatory] New name for the tag
# - DOCKER_LOGIN_USERNAME: Username to log into the docker registry. Defaults to the CI Runner account
# - DOCKER_LOGIN_PASSWORD: Password to log into the docker registry. Default to the CI Token
# - DOCKER_IMAGE: The docker image. Defaults to $CI_REGISTRY/$CI_PROJECT_PATH
#      environment variables, which in Gitlab-CI is the current registry/repository, i.e.: gitlab-registry.cern.ch/group/project

# Exit codes:
# 0: All good
# 1: OLDTAG and/or NEWTAG are not set

set -e

DOCKER_LOGIN_USERNAME=${DOCKER_LOGIN_USERNAME:-gitlab-ci-token}
DOCKER_LOGIN_PASSWORD=${DOCKER_LOGIN_PASSWORD:-${CI_JOB_TOKEN}}
DOCKER_IMAGE=${DOCKER_IMAGE_PATH:-${CI_REGISTRY}/${CI_PROJECT_PATH}}
#

# Source common variables and functions
. /usr/local/bin/common.sh

DOCKER_IMAGE_PATH=$(extractDockerImagePath $DOCKER_IMAGE)
DOCKER_SERVER=$(extractDockerServer $DOCKER_IMAGE)

# workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380
if [ -e /.runonce ]; then
  echo "[gitlabci-docker-tools] Build already done (workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380)"
  exit 0
fi
touch /.runonce

if [ -z "${OLDTAG}" ] || [ -z "${NEWTAG}" ]
then
    echo "Please set OLDTAG and NEWTAG to make the script work. See README.md for more information"
    exit 1
fi

#
# In order to get the TOKEN, we need to make a NON authenticated request. A Www-Authenticate header
#     will be returned with the login details
#
CODE=$(curl -s https://$DOCKER_SERVER/v2/$DOCKER_IMAGE_PATH/manifests/${OLDTAG} -D headers.txt \
-o manifest.json -H "Accept: $CONTENT_TYPE" -w "%{http_code}")

#
# If the request returns 401 - Unauthorized, we use the WWW-Authenticate header stored in a file to
#     obtain a token from the Docker registry. This token can then be used to perform requests that
#      require authentication.
#
if [ "$CODE" == "401" ];
then
    echo "Getting the token"
    TOKEN=$(getToken headers.txt)

    curl -H "Authorization: Bearer $TOKEN" https://$DOCKER_SERVER/v2/$DOCKER_IMAGE_PATH/manifests/${OLDTAG} \
        -H "Accept: $CONTENT_TYPE" -o manifest.json -s
fi

curl https://$DOCKER_SERVER/v2/$DOCKER_IMAGE_PATH/manifests/${NEWTAG} -X PUT \
    -H "Content-type: $CONTENT_TYPE" -H "Authorization: Bearer ${TOKEN}" -d"@manifest.json"

echo "The image ${DOCKER_IMAGE_PATH}:${NEWTAG} has been successfully pushed to the registry"
