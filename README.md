# Docker image tools

## Summary

This repository contains several tools to interact with the GitLab registry. All these operations can be run on the shared standard GitLab-CI runners

## Operations

### Add a tag

From a Docker Image already pushed to the repository, adds a new tag to it. This way the same image can have two tags (latest and stable).

#### Environment variables

`OLDTAG`: [**Mandatory**] A tag already pushed to the registry.

`NEWTAG`: [**Mandatory**] New tag to be set to the image referenced with `OLDTAG`.

`DOCKER_IMAGE_PATH` : Path where the docker image is stored. Defaults to the `CI_PROJECT_PATH` environment variable, which in Gitlab-CI is the current repository. Shortnames are not supported, i.e.: `alpine` o `ubuntu`. It expects to have an `/` in its name.

`DOCKER_SERVER`: A docker registry to login into (allows to provide credentials for pushing the image). By default: gitlab-registry.cern.ch ($CI_REGISTRY).

`DOCKER_LOGIN_USERNAME`: Username to log into the docker registry. Defaults to the CI Runner  account

`DOCKER_LOGIN_PASSWORD`: Password to log into the docker registry. Default to the CI Token

#### Examples

The following GitLab-CI job would add a new tag to an already existing image `$CI_PROJECT_PATH:custom_feature_1` to `$CI_PROJECT_PATH:stable`

```yaml
add_tag_apply_operation:
  image: gitlab-registry.cern.ch/ci-tools/docker-image-tools:add-tag
  script:
  - 'Adding tag...'
  variables:
    OLDTAG: custom_feature_1
    NEWTAG: stable
```

### Delete an image by tag

Removes a Docker image from the GitLab registry, from a tag name.

Warning: in the docker registry, one can only delete an image, and in turn this removes any tag that references this image. For instance if we
have tag A, rename it to tag B, then delete the image referenced by tag A, tag B will be deleted as well because it points to the same image.
(Unless something else was pushed to A in between)

#### Environment variables

`TAG`: [**Mandatory**] Tag pointing to the image to be deleted

`DOCKER_LOGIN_PASSWORD`: [**Mandatory**] Password/Token for authentication against the Docker Registry. Defaults to the CI Token of the job. Due to https://gitlab.com/gitlab-org/gitlab-ce/issues/51769, the user must provide either a password or a GitLab Personal Access Token in order to delete an image from the registry. When using a PAT, DOCKER_LOGIN_USERNAME is not needed.

`DOCKER_LOGIN_USERNAME`: Username to log into the docker registry. Defaults to the CI Runner account

`DOCKER_IMAGE_PATH`: Path where the docker image is stored. Only the Gitlab registry is supported. Defaults to the $CI_PROJECT_PATH environment variable, which in Gitlab-CI is the current repository

#### Examples

The following example will delete the `custom_feature_1` tag in the current Docker repository in the registry (ie the image `$CI_PROJECT_PATH:custom_feature_1`)

```yaml
delete_image_apply_operation:
  image: gitlab-registry.cern.ch/ci-tools/docker-image-tools:delete-image
  script:
  - 'Deleting image...'
  variables:
    TAG: custom_feature_1
```
