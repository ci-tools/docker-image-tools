FROM alpine:latest

# In order to reuse the same Dockerfile for several images,
# pass the running script as a build-arg
ARG script

COPY $script /usr/local/bin/operation.sh
COPY common.sh /usr/local/bin/common.sh

# Install curl to query the gitlab-registry
# Install python to urlencode docker digest
RUN apk add --update curl python jq && \
    rm -rf /var/cache/apk/* && \
    # Set operation script to be runnable
    chmod 755 /usr/local/bin/operation.sh /usr/local/bin/common.sh

ENTRYPOINT ["operation.sh"]
